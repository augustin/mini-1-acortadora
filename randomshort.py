import webapp

formulario = """
    <form action="" method="POST">
            <p>Introduce URL: <input type="text" name="url"/>
                URL acortada: <input type="text" name="acortada"/>
                <input type="submit" value="Enviar url"/>
    </form>

"""


class randomshort(webapp.webApp):
    urls: dict = {}

    def parse(self, request):
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        resto = request.split('\r\n\r\n', 1)[1]

        return metodo, recurso, resto

    def printURLS(self) -> str:
        url: str = ""
        for urlAux in self.urls:
            url = url + '<br>URL: ' + self.urls[urlAux] + '<b>, URL acortada: </b>' + urlAux
        return url

    def process(self, resourceName):
        metodo, recurso, resto = resourceName

        if metodo == "GET":
            if recurso == "/":
                httpCode = "200 ok"
                htmlBody = "<html><body><p>" + formulario + "<p>Urls y URLs acotadas: </p>" + \
                           "<p>" + self.printURLS() + "</p>" + \
                           "</body></html>"
            elif recurso in self.urls:
                httpCode = "200 ok"
                htmlBody = "<html><body><p>Urls y URLs acotadas: </p> <meta http-equiv='refresh' content='2;" + \
                           "URL=" + self.urls[recurso] + "'></body></html>"
            else:
                httpCode = "404 not Found"
                htmlBody = "<html><body><p>Recurso: " + recurso + \
                           " no encontrado</body></html>"

            return httpCode, htmlBody

        if metodo == "POST":
            contenido = resto.split('&')
            urlaux = contenido[0].split('=')[1]
            contenidoAux = contenido[1].split('=')[1]
            url = urlaux.replace('%3A', ':').replace('%2F', '/')

            if url == "" and contenidoAux == "":
                httpCode = "404 not Found"
                htmlBody = "<html><body><p>Error, debe rellenar los campos</p></body></html>"

                return httpCode, htmlBody

            if url[:7] != "http://" and url[:8] != "https://":
                url = "https://" + url
                aux = '/' + contenidoAux
                self.urls[aux] = url

                httpCode = "200 OK"
                htmlBody = "<html><body>URL: " + "<a href='" + url + \
                           "'>" + url + "</a><br>" + "Acortada: " + \
                           "<a href='" + self.urls[aux] + "'>" + \
                           aux + "</a></body></html>"

                return httpCode, htmlBody

            else:
                aux = '/' + contenidoAux
                self.urls[aux] = url
                httpCode = "200 OK"
                htmlBody = "<html><body>url: " + "<a href='" + url + \
                           "'>" + url + "</a><br>" + "Short: " + \
                           "<a href='" + self.urls[aux] + "'>" + \
                           aux + "</a></body></html>"

                return httpCode, htmlBody



if __name__ == "__main__":
    testWebApp = randomshort("localhost", 1234)
    
